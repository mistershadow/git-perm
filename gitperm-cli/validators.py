from PyInquirer import Validator, ValidationError
import gitlab
from database_manager import DatabaseManager
from choice_manager import ChoiceManager


class GitlabTokenValidator(Validator):
    def validate(self, token):
        gl = gitlab.Gitlab("https://gitlab.com", private_token=token.text)
        try:
            gl.auth()
        except gitlab.GitlabAuthenticationError:
            raise ValidationError(
                message="Token is invalid!", cursor_position=len(token.text)
            )


class GroupValidator(Validator):
    def validate(self, name):
        if not DatabaseManager.does_group_exist(
            name.text, ChoiceManager.get_curr_project_id()
        ):
            if not name.text.isalnum():
                raise ValidationError(
                    message="Group name must be Alphanumeric!",
                    cursor_position=len(name.text),
                )
        else:
            raise ValidationError(
                message="Group already exists!", cursor_position=len(name.text)
            )


class FilePatternValidator(Validator):
    def validate(self, pattern):
        if len(pattern.text) == 0:
            raise ValidationError(
                message="File Pattern cannot be blank!",
                cursor_position=len(pattern.text),
            )

        if pattern.text in [
            permitted_pattern["pattern"]
            for permitted_pattern in DatabaseManager.get_group_permitted_file_patterns(
                ChoiceManager.get_curr_group_id()
            )
        ]:
            raise ValidationError(
                message="Pattern already exists!", cursor_position=len(pattern.text)
            )
