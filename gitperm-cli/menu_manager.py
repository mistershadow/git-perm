from choice_manager import ChoiceManager
import sys
from pyfiglet import figlet_format
from termcolor import cprint


class MenuManager:
    def __init__(self):
        self.next_choice = ChoiceManager.input_token

    def run_menu(self):
        cprint(
            figlet_format("Git Perm", font="slant"), "yellow", attrs=["bold"], end="",
        )

        while self.next_choice is not None:
            try:
                self.next_choice = self.next_choice()
            except KeyError:
                sys.exit(1)
