import sys
from termcolor import cprint
import colorama
import pymongo.errors
import requests.exceptions

colorama.init()


def global_exception_handler(exctype, value, traceback):
    if (
        exctype == pymongo.errors.ServerSelectionTimeoutError
        or exctype == pymongo.errors.AutoReconnect
    ):
        cprint(
            "Cannot access MongoDB. Make sure that the docker-compose is running.",
            "red",
            attrs=["bold"],
        )
    elif exctype == requests.exceptions.ConnectionError:
        cprint(
            "Cannot access GitLab server. Make sure that you have an active internet connection.",
            "red",
            attrs=["bold"],
        )
    else:
        sys.__excepthook__(exctype, value, traceback)


def main():
    from menu_manager import MenuManager

    menu_manager = MenuManager()
    menu_manager.run_menu()


if __name__ == "__main__":
    sys.excepthook = global_exception_handler
    main()
