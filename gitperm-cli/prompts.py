from validators import GitlabTokenValidator, GroupValidator, FilePatternValidator

from choice_manager import ChoiceManager

token_prompt = {
    "type": "password",
    "name": "token",
    "message": "Enter GitLab Personal Access Token (With API Access Scope)",
    "validate": GitlabTokenValidator,
}

projects_prompt = {
    "type": "list",
    "name": "project",
    "message": "Choose Projects to View/Edit Permissions:",
}

actions_menu_prompts = {
    "type": "list",
    "name": "action",
    "message": "Choose Action:",
    "choices": lambda _: ChoiceManager.get_actions(),
}

create_group_prompt = {
    "type": "input",
    "name": "group_name",
    "message": "Enter Permission Group Name:",
    "validate": GroupValidator,
}

confirm_create_group_prompt = {
    "type": "confirm",
    "name": "create_group",
    "format": 'Create Permission Group "%s"?',
    "default": False,
}

delete_group_prompt = {
    "type": "list",
    "name": "group_info",
    "message": "Choose Permission Group to Delete:",
}

confirm_delete_group_prompt = {
    "type": "confirm",
    "name": "delete_group",
    "format": 'Delete Permission Group "%s"?',
    "default": False,
}

edit_group_users_prompt = {
    "type": "list",
    "name": "group_info",
    "message": "Choose Permission Group to Manage Users:",
}

assign_group_users_prompt = {
    "type": "checkbox",
    "name": "user_ids",
    "message": "Choose Users to Assign to This Group:",
}

group_permissions_prompt = {
    "type": "list",
    "name": "group_name",
    "message": "Choose Group to Manage Permissions:",
}

manage_permissions_prompt = {
    "type": "list",
    "name": "action",
    "message": "Choose Permissions to Manage:",
    "choices": lambda _: ChoiceManager.get_permission_choices(),
}

manage_protected_branches_prompt = {
    "type": "checkbox",
    "name": "permitted_branches",
    "message": "Select Permitted Branches for Group:",
}

manage_permitted_file_patterns_prompt = {
    "type": "list",
    "name": "action",
    "message": "Choose Action",
    "choices": lambda _: ChoiceManager.get_file_permission_actions(),
}

add_permitted_file_pattern_prompt = {
    "type": "input",
    "name": "pattern",
    "message": "Enter Permitted Files Pattern (Unix Shell-Style):",
    "validate": FilePatternValidator,
}

choose_permitted_file_pattern_to_manage = {
    "type": "list",
    "name": "pattern",
    "message": "Choose Permitted File Pattern to Manage File Statuses:",
}

manage_permitted_statuses_for_pattern_prompt = {
    "type": "checkbox",
    "name": "permitted_statuses",
    "message": "Select Permitted Statuses for Staged Files of this Pattern:",
    "filter": (
        lambda permitted_statuses: [
            status[ChoiceManager.STATUS_LETTER] for status in permitted_statuses
        ]
    ),
}

remove_permitted_file_pattern_prompt = {
    "type": "list",
    "name": "pattern",
    "message": "Select Permitted File Patterns to Remove:",
}

confirm_remove_permitted_file_pattern_prompt = {
    "type": "confirm",
    "name": "remove_pattern",
    "format": "Remove Permitted File Pattern <%s>?",
    "default": False,
}
