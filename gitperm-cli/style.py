from PyInquirer import Token, style_from_dict

common_style = style_from_dict(
    {
        Token.QuestionMark: "#e5062b bold",
        Token.Selected: "bold",
        Token.Instruction: "",  # default
        Token.Answer: "bold",
        Token.Question: "#f44d27 bold",
        Token.Pointer: "#f44d27",
    }
)
