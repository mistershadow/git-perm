import pymongo
import pymongo.errors


class DatabaseManager:

    URI = "mongodb://localhost:27017"

    client = pymongo.MongoClient(URI, serverSelectionTimeoutMS=5000)

    db = client["gitperm"]

    projects = db["projects"]
    groups = db["groups"]
    users = db["users"]

    projects.create_index("url", unique=True)
    users.create_index("user_id", unique=True)

    @classmethod
    def add_project(cls, project_url, project_name):
        try:
            cls.projects.insert_one({"url": project_url, "name": project_name})
        except pymongo.errors.DuplicateKeyError:
            pass

    @classmethod
    def init_users(cls, users):
        for user in users:
            cls.users.update_one(
                {"user_id": user["user_id"]},
                {
                    "$setOnInsert": {
                        "user_id": user["user_id"],
                        "username": user["username"],
                        "name": user["name"],
                    }
                },
                upsert=True,
            )

    @classmethod
    def assign_users_to_group(cls, user_ids, group_id):
        for user_id in user_ids:
            cls.users.update_one(
                {"user_id": user_id, "assigned_groups": {"$nin": [group_id]}},
                {"$push": {"assigned_groups": group_id}},
            )

    @classmethod
    def revoke_all_users_from_group(cls, group_id):
        cls.users.update_many(
            {"assigned_groups": {"$in": [group_id]}},
            {"$pull": {"assigned_groups": group_id}},
        )

    @classmethod
    def get_group_users(cls, group_id):
        return cls.users.find({"assigned_groups": {"$in": [group_id]}})

    @classmethod
    def is_user_in_group(cls, user_id, group_id):
        return (
            cls.users.count_documents(
                {"user_id": user_id, "assigned_groups": {"$in": [group_id]}}
            )
            != 0
        )

    @classmethod
    def get_groups(cls, project_id):
        return cls.groups.find({"project": project_id})

    @classmethod
    def add_group(cls, name, project_id):
        try:
            cls.groups.insert_one({"name": name, "project": project_id})
        except pymongo.errors.DuplicateKeyError:
            pass

    @classmethod
    def remove_group(cls, group_id):
        cls.revoke_all_users_from_group(group_id)
        cls.groups.delete_one({"_id": group_id})

    @classmethod
    def permit_branches(cls, branches, group_id):
        for branch in branches:
            cls.groups.update_one(
                {"_id": group_id, "permitted_branches": {"$nin": [branch]}},
                {"$push": {"permitted_branches": branch}},
            )

    @classmethod
    def protect_all_branches(cls, group_id):
        cls.groups.update_one({"_id": group_id}, {"$unset": {"permitted_branches": ""}})

    @classmethod
    def is_branch_permitted(cls, branch, group_id):
        return (
            cls.groups.count_documents(
                {"_id": group_id, "permitted_branches": {"$in": [branch]}}
            )
            != 0
        )

    @classmethod
    def get_group_permitted_file_patterns(cls, group_id):
        try:
            return cls.groups.find_one({"_id": group_id})["permitted_file_patterns"]
        except KeyError:
            return []

    @classmethod
    def get_file_pattern_permitted_statuses(cls, group_id, pattern):
        try:
            return cls.groups.find_one(
                {"_id": group_id},
                {"permitted_file_patterns": {"$elemMatch": {"pattern": pattern}}},
            )["permitted_file_patterns"][0]["permitted_statuses"]
        except (KeyError, IndexError):
            return []

    @classmethod
    def add_permitted_file_pattern(cls, group_id, pattern, permitted_statuses):
        cls.groups.update_one(
            {"_id": group_id},
            {
                "$push": {
                    "permitted_file_patterns": {
                        "pattern": pattern,
                        "permitted_statuses": permitted_statuses,
                    }
                }
            },
        )

    @classmethod
    def remove_permitted_file_pattern(cls, group_id, pattern):
        cls.groups.update_one(
            {"_id": group_id, "permitted_file_patterns.pattern": pattern,},
            {"$pull": {"permitted_file_patterns": {"pattern": pattern}}},
        )

    @classmethod
    def does_group_exist(cls, name, project_id):
        return cls.groups.count_documents({"name": name, "project": project_id}) != 0

    @classmethod
    def get_project_id(cls, project_url):
        return cls.projects.find_one({"url": project_url})["_id"]
