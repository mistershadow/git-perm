from PyInquirer import prompt
from style import common_style
from gitlab_manager import GitlabManager
from database_manager import DatabaseManager
from termcolor import cprint


class ChoiceManager:

    gitlab_manager = None

    curr_project = None
    _curr_project_id = None

    curr_group_info = None

    STATUS_LETTER = 1

    @classmethod
    def input_token(cls):
        from prompts import token_prompt

        token = cls.prompt_for_answer(token_prompt)

        cls.create_gitlab_manager(token)
        return cls.choose_project

    @classmethod
    def choose_project(cls):
        from prompts import projects_prompt

        projects_prompt[
            "choices"
        ] = lambda _: ChoiceManager.gitlab_manager.get_project_choices()

        cls._curr_project_id = None
        cls.curr_project = cls.prompt_for_answer(projects_prompt)

        if cls.curr_project:
            DatabaseManager.add_project(
                cls.get_curr_project_url(), cls.get_curr_project_name()
            )

            DatabaseManager.init_users(
                cls.gitlab_manager.get_project_users(cls.curr_project)
            )

            return cls.choose_action

    @classmethod
    def choose_action(cls):
        from prompts import actions_menu_prompts

        return cls.prompt_for_answer(actions_menu_prompts)

    @classmethod
    def list_permission_groups(cls):
        for group in DatabaseManager.get_groups(cls.get_curr_project_id()):
            print("   ", end="")
            cprint("{}:".format(group["name"]), "yellow", attrs=["bold", "underline"])
            for user in DatabaseManager.get_group_users(group["_id"]):
                print("    - {}".format(user["name"]))

        return cls.choose_action

    @classmethod
    def create_permission_group(cls):
        from prompts import create_group_prompt, confirm_create_group_prompt

        group_name = cls.prompt_for_answer(create_group_prompt)

        confirm_create_group_prompt["message"] = (
            confirm_create_group_prompt["format"] % group_name
        )

        create_group = cls.prompt_for_answer(confirm_create_group_prompt)

        if create_group:
            DatabaseManager.add_group(group_name, cls.get_curr_project_id())

        return cls.choose_action

    @classmethod
    def remove_permission_group(cls):
        from prompts import delete_group_prompt, confirm_delete_group_prompt

        cls.select_group(delete_group_prompt)

        if cls.curr_group_info:

            confirm_delete_group_prompt["message"] = (
                confirm_delete_group_prompt["format"] % cls.curr_group_info["name"]
            )

            delete_group = cls.prompt_for_answer(confirm_delete_group_prompt)

            if delete_group:
                DatabaseManager.remove_group(cls.get_curr_group_id())

            return cls.remove_permission_group

        return cls.choose_action

    @classmethod
    def manage_permission_group_users(cls):
        from prompts import edit_group_users_prompt, assign_group_users_prompt

        cls.select_group(edit_group_users_prompt)

        if cls.curr_group_info:
            assign_group_users_prompt[
                "choices"
            ] = lambda _: cls.get_group_user_choices()

            assigned_users_names = cls.prompt_for_answer(assign_group_users_prompt)

            assigned_user_ids = []

            project_users = cls.gitlab_manager.get_project_users(cls.curr_project)

            for assigned_user_name in assigned_users_names:
                for user in project_users:
                    if assigned_user_name == user["name"]:
                        assigned_user_ids.append(user["user_id"])
                        break

            DatabaseManager.revoke_all_users_from_group(cls.get_curr_group_id())

            DatabaseManager.assign_users_to_group(
                assigned_user_ids, cls.get_curr_group_id()
            )

            return cls.manage_permission_group_users

        return cls.choose_action

    @classmethod
    def manage_group_permissions(cls):
        from prompts import group_permissions_prompt

        cls.select_group(group_permissions_prompt)

        if not cls.curr_group_info:
            return cls.choose_action

        return cls.choose_permission_to_manage

    @classmethod
    def choose_permission_to_manage(cls):
        from prompts import manage_permissions_prompt

        return cls.prompt_for_answer(manage_permissions_prompt)

    @classmethod
    def manage_permitted_branches(cls):
        from prompts import manage_protected_branches_prompt

        manage_protected_branches_prompt[
            "choices"
        ] = lambda _: cls.gitlab_manager.get_branch_choices(
            cls.curr_project, cls.get_curr_group_id()
        )

        permitted_branches = cls.prompt_for_answer(manage_protected_branches_prompt)

        DatabaseManager.protect_all_branches(cls.get_curr_group_id())

        DatabaseManager.permit_branches(permitted_branches, cls.get_curr_group_id())

        return cls.choose_permission_to_manage

    @classmethod
    def manage_permitted_file_patterns(cls):
        from prompts import manage_permitted_file_patterns_prompt

        return cls.prompt_for_answer(manage_permitted_file_patterns_prompt)

    @classmethod
    def list_permitted_file_patterns(cls):
        for permitted_file_pattern in DatabaseManager.get_group_permitted_file_patterns(
            cls.get_curr_group_id()
        ):
            cprint(
                "   {}".format(permitted_file_pattern["pattern"]),
                "yellow",
                attrs=["bold"],
                end=" ",
            )
            print("[{}]".format("".join(permitted_file_pattern["permitted_statuses"])))

        return cls.manage_permitted_file_patterns()

    @classmethod
    def add_permitted_file_pattern(cls):
        from prompts import add_permitted_file_pattern_prompt

        pattern = cls.prompt_for_answer(add_permitted_file_pattern_prompt)

        return lambda: cls.manage_permitted_statuses_for_pattern(pattern)

    @classmethod
    def remove_permitted_file_pattern(cls):
        from prompts import (
            remove_permitted_file_pattern_prompt,
            confirm_remove_permitted_file_pattern_prompt,
        )

        remove_permitted_file_pattern_prompt[
            "choices"
        ] = lambda _: cls.get_group_file_permission_pattern_choices()

        pattern = cls.prompt_for_answer(remove_permitted_file_pattern_prompt)

        if pattern:

            confirm_remove_permitted_file_pattern_prompt["message"] = (
                confirm_remove_permitted_file_pattern_prompt["format"] % pattern
            )

            remove_pattern = cls.prompt_for_answer(
                confirm_remove_permitted_file_pattern_prompt
            )

            if remove_pattern:
                DatabaseManager.remove_permitted_file_pattern(
                    cls.get_curr_group_id(), pattern
                )

            return cls.remove_permitted_file_pattern

        return cls.manage_permitted_file_patterns

    @classmethod
    def choose_permitted_file_pattern_to_manage_statuses(cls):
        from prompts import choose_permitted_file_pattern_to_manage

        choose_permitted_file_pattern_to_manage[
            "choices"
        ] = lambda _: cls.get_group_file_permission_pattern_choices()

        pattern = cls.prompt_for_answer(choose_permitted_file_pattern_to_manage)

        if pattern:
            return lambda: cls.manage_permitted_statuses_for_pattern(pattern)

        return cls.manage_permitted_file_patterns

    @classmethod
    def manage_permitted_statuses_for_pattern(cls, pattern):
        from prompts import manage_permitted_statuses_for_pattern_prompt

        manage_permitted_statuses_for_pattern_prompt[
            "choices"
        ] = lambda _: cls.get_available_file_statuses(pattern)

        permitted_statuses = cls.prompt_for_answer(
            manage_permitted_statuses_for_pattern_prompt
        )

        DatabaseManager.remove_permitted_file_pattern(cls.get_curr_group_id(), pattern)

        DatabaseManager.add_permitted_file_pattern(
            cls.get_curr_group_id(), pattern, permitted_statuses
        )

        return cls.choose_permitted_file_pattern_to_manage_statuses

    @classmethod
    def select_group(cls, groups_prompt):
        groups_prompt["choices"] = lambda _: ChoiceManager.get_group_choices()

        cls.curr_group_info = cls.prompt_for_answer(groups_prompt)

    @classmethod
    def create_gitlab_manager(cls, token):
        cls.gitlab_manager = GitlabManager(token)

    @classmethod
    def get_actions(cls):
        return [
            {"name": "List Permission Groups", "value": cls.list_permission_groups},
            {"name": "Add Permission Group", "value": cls.create_permission_group},
            {"name": "Remove Permission Group", "value": cls.remove_permission_group},
            {
                "name": "Manage Permission Group Users",
                "value": cls.manage_permission_group_users,
            },
            {"name": "Manage Group Permissions", "value": cls.manage_group_permissions},
            {"name": "Back", "value": cls.choose_project},
        ]

    @classmethod
    def get_permission_choices(cls):
        return [
            {"name": "Permitted Branches", "value": cls.manage_permitted_branches},
            {
                "name": "Permitted File Patterns",
                "value": cls.manage_permitted_file_patterns,
            },
            {"name": "Back", "value": cls.manage_group_permissions},
        ]

    @classmethod
    def get_file_permission_actions(cls):
        return [
            {
                "name": "List Permitted File Patterns",
                "value": cls.list_permitted_file_patterns,
            },
            {
                "name": "Add Permitted File Pattern",
                "value": cls.add_permitted_file_pattern,
            },
            {
                "name": "Manage Permitted Statuses for File Pattern",
                "value": cls.choose_permitted_file_pattern_to_manage_statuses,
            },
            {
                "name": "Remove Permitted File Pattern",
                "value": cls.remove_permitted_file_pattern,
            },
            {"name": "Back", "value": cls.choose_permission_to_manage},
        ]

    @classmethod
    def get_available_file_statuses(cls, pattern):
        statuses = [
            {"name": "[A] Added"},
            {"name": "[C] Copied"},
            {"name": "[D] Deleted"},
            {"name": "[M] Modified"},
            {"name": "[R] Renamed"},
            {"name": "[T] Type Changed"},
            {"name": "[U] Unmerged"},
            {"name": "[X] Unknown"},
            {"name": "[B] Pairing Broken"},
        ]

        permitted_statuses = DatabaseManager.get_file_pattern_permitted_statuses(
            cls.get_curr_group_id(), pattern
        )

        for status in statuses:
            status["checked"] = status["name"][cls.STATUS_LETTER] in permitted_statuses

        return statuses

    @classmethod
    def get_curr_group_id(cls):
        return cls.curr_group_info["id"]

    @classmethod
    def get_curr_project_id(cls):
        if not cls._curr_project_id:
            cls._curr_project_id = DatabaseManager.get_project_id(
                cls.get_curr_project_url()
            )
        return cls._curr_project_id

    @classmethod
    def get_curr_project_url(cls):
        return cls.curr_project.attributes["http_url_to_repo"]

    @classmethod
    def get_curr_project_name(cls):
        return cls.curr_project.attributes["name_with_namespace"]

    @classmethod
    def get_group_choices(cls):
        return [
            {
                "name": group["name"],
                "value": {"name": group["name"], "id": group["_id"]},
            }
            for group in DatabaseManager.get_groups(cls.get_curr_project_id())
        ] + [{"name": "Back", "value": False}]

    @classmethod
    def get_group_user_choices(cls):
        project_users = cls.gitlab_manager.get_project_users(cls.curr_project)

        return [
            {
                "name": user["name"],
                "checked": DatabaseManager.is_user_in_group(
                    user["user_id"], cls.get_curr_group_id()
                ),
            }
            for user in project_users
        ]

    @classmethod
    def get_group_file_permission_pattern_choices(cls):
        return [
            pattern["pattern"]
            for pattern in DatabaseManager.get_group_permitted_file_patterns(
                cls.get_curr_group_id()
            )
        ] + [{"name": "Back", "value": False}]

    @staticmethod
    def prompt_for_answer(prompt_object):
        answer = prompt(prompt_object, style=common_style)
        return answer[prompt_object["name"]]

    @staticmethod
    def prompt_for_answers(prompt_object):
        answers = prompt(prompt_object, style=common_style)
        return answers
