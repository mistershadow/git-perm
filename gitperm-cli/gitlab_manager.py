import gitlab
from database_manager import DatabaseManager


class GitlabManager:

    URL = "https://gitlab.com"

    def __init__(self, token):
        self.gl = gitlab.Gitlab(url=self.URL, private_token=token)
        self._projects = None

    @property
    def projects(self):
        if not self._projects:
            self._projects = self.gl.projects.list(owned=True)
        return self._projects

    def get_project_choices(self):
        return [
            {"name": project.attributes["name_with_namespace"], "value": project}
            for project in self.projects
        ] + [{"name": "Exit", "value": False}]

    @staticmethod
    def get_branch_choices(project, group_id):
        return [
            {
                "name": branch.attributes["name"],
                "checked": DatabaseManager.is_branch_permitted(
                    branch.attributes["name"], group_id
                ),
            }
            for branch in project.branches.list()
        ]

    @staticmethod
    def get_project_users(project):
        return [
            {
                "user_id": user.attributes["id"],
                "username": user.attributes["username"],
                "name": user.attributes["name"],
            }
            for user in project.members.all(all=True)
        ]
