<!-- PROJECT LOGO -->
<br />
<div align="center">
      <a href="https://gitlab.com/mistershadow/git-perm/">
        <img src="images/gitperm.png" alt="Logo" width="150" height="150">
      </a>
</div>


<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)

<!-- ABOUT THE PROJECT -->
## About The Project

GitPerm is a locally managed permission system for GitLab - allowing you to create
permission groups and set permissions for your owned projects. GitPerm uses a
cli-tool to manage permissions and permission groups, a Mongo database to store
permission information and a contanerized server to listen for permission requets,
which returns permission violations for a requested commit whilst logging them on
the server side.

Gitlab features the following:
* CLI-Tool that manages all permission handling
* Server Image that hosts the permission verification server
* Docker-Compose that quickly gets the server and db running
* Integration with GitLab API to get repository information
* Client sided pre-commit hook to capture commits
* Permission groups and assigning of project members to them
* Permitted file patterns and setting allowed stages statuses to them
* Branch protection

### Built With
* [Python GitLab](https://github.com/python-gitlab/python-gitlab)
* [PyInquirer](https://github.com/CITGuru/PyInquirer)
* [PyMongo](https://github.com/mongodb/mongo-python-driver)
* [Docker](https://www.docker.com/)

<!-- GETTING STARTED -->
## Getting Started

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Prerequisites

* python3
```sh
$ sudo apt-get update
$ sudo apt-get install python3
```
* [docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
* docker-compose
```sh
$ sudo apt install docker-compose
```

### Installation

1. [Create GitLab Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token)  (with API scope)
2. Clone the repo
```sh
git clone https://gitlab.com/mistershadow/git-perm.git
```
3. Install requirements for the CLI-Tool
```sh
$ pip3 install -r ./gitperm-cli/requirements.txt
```
4. Copy pre-commit hook to repository of choice
```sh
$ cp ./gitperm-hooks/pre-commit /path/to/repo/.git/hooks
```


<!-- USAGE EXAMPLES -->
## Usage

1. Run the docker-compose to setup the server and database
```sh
$ docker-compose up --build
```
2. To invoke the cli-tool and manage permissions you should run it with python
```sh
$ python3 ./gitperm-cli/main.py
```
3. Insert the API key into the CLI-Tool
4. Choose one of your owned projects to set permissions

    ![Project Selection](images/project-selection.png)

5. Add at least one permission group and assign project users to it

    ![User Selection](images/group-users.png)

6. Set permissions for currently added group:
   * Add permitted branches to restrict group to only commit to those branches
   
     ![Branch Permissions](images/branch-permissions.png)

   * Add permitted file patterns and assign permitted stage statuses
     - Permitted file staged statuses are the allowed action a user can do on a file for staging
     
       ![File Permissions](images/file-permissions.png)

7. Stage changed and commit in the chosen repository with the pre-commit hook
8. Commit changes and login into your GitLab accout
    * If commit passes through without violationg permission you will see the following:

       ![Permitted](images/permitted.png)
       
    * If you have violated permissions the commit will no go through and you'll see an explanation of what failed
    
       ![Denied](images/denied.png)
       
9. The docker-compose will log all violations and permitted commits as shown here:

    ![Logs](images/logs.png)