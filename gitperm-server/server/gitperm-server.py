import selectors
import socket
import types
import pickle
import violations
import logging
from logging_formatter import LoggingFormatter

logger = logging.getLogger("gitperm-logger")
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

ch.setFormatter(LoggingFormatter())

logger.addHandler(ch)

HOST = "0.0.0.0"
PORT = 1337
BUFSIZE = 4096


def accept_connection(sock):
    conn, addr = sock.accept()
    logger.debug("Accepted connection from {0}".format(addr))
    conn.setblocking(False)
    data = types.SimpleNamespace(addr=addr, inb=b"", outb=b"", received=False)
    events = selectors.EVENT_READ | selectors.EVENT_WRITE
    sel.register(conn, events, data=data)


def serve_connection(key, mask):
    sock = key.fileobj
    data = key.data
    if mask & selectors.EVENT_READ and not data.received:
        recv_data = sock.recv(BUFSIZE)

        data.inb += recv_data

        if len(recv_data) < BUFSIZE:
            logger.debug("Received request from {0}".format(data.addr))

            data.received = True

            command_data = pickle.loads(data.inb)

            violations_dict = violations.get_violations(
                command_data["user_id"],
                command_data["project_url"],
                command_data["touched_branches"],
                command_data["staged_files"],
            )

            username = command_data["username"]

            if not violations_dict["violated"]:
                logger.info("@{}: Passed commit violations check.".format(username))
            else:
                for violation in violations_dict["verbose_violations"]:
                    logger.critical("@{}: {}".format(username, violation))

            data.outb = pickle.dumps(violations_dict)
    if mask & selectors.EVENT_WRITE and data.outb:
        logger.debug("Sending violations to {0}".format(data.addr))
        sent = sock.send(data.outb)
        data.outb = data.outb[sent:]
    if data.received and not data.outb:
        logger.debug("Closing connection to {0}".format(data.addr))
        sel.unregister(sock)
        sock.close()


def main():
    lsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    lsock.bind((HOST, PORT))
    lsock.listen()

    logger.debug("Listening on {0}".format((HOST, PORT)))

    lsock.setblocking(False)
    sel.register(lsock, selectors.EVENT_READ, data=None)

    while True:
        events = sel.select(timeout=None)
        for key, mask in events:
            if key.data is None:
                accept_connection(key.fileobj)
            else:
                serve_connection(key, mask)


if __name__ == "__main__":
    sel = selectors.DefaultSelector()

    main()
