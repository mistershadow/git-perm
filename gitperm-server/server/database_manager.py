import pymongo


class DatabaseManager:

    URI = "mongodb://gp-db:27017"

    client = pymongo.MongoClient(URI)
    db = client["gitperm"]

    projects = db["projects"]
    groups = db["groups"]
    users = db["users"]

    @classmethod
    def get_user_permitted_branches(cls, user_id, project_url):
        permissions_array = "permitted_branches"

        try:
            return cls.get_user_grouped_permissions(
                user_id,
                project_url,
                permissions_array,
                cls.get_user_permitted_branches_merge_stages(),
            ).next()[permissions_array]
        except StopIteration:
            return []

    @classmethod
    def get_user_permitted_file_patterns(cls, user_id, project_url):
        return list(
            cls.get_user_grouped_permissions(
                user_id,
                project_url,
                "permitted_file_patterns",
                cls.get_user_permitted_file_patterns_merge_stages(),
            )
        )

    @classmethod
    def get_user_permitted_branches_merge_stages(cls):
        return [cls.merge_permissions_stage("permitted_branches")]

    @classmethod
    def get_user_permitted_file_patterns_merge_stages(cls):
        return [
            cls.replace_document_by_pattern_document(),
            cls.group_permissions_by_pattern(),
            cls.expand_and_merge_status_arrays(),
        ]

    @classmethod
    def get_user_grouped_permissions(
        cls, user_id, project_url, permissions_array, merge_stages
    ):
        return cls.users.aggregate(
            cls.get_user_pre_merged_permissions_pipeline(
                user_id, project_url, permissions_array
            )
            + merge_stages
        )

    @classmethod
    def get_user_pre_merged_permissions_pipeline(
        cls, user_id, project_url, permissions_array
    ):
        return [
            cls.match_user_id_stage(user_id),
            cls.lookup_group_objects_stage(),
            cls.unwind_group_objects_stage(),
            cls.lookup_project_object_for_groups_stage(),
            cls.group_permissions_by_project_stage(permissions_array),
            cls.match_project_url_stage(project_url),
            cls.unwind_permissions_array_stage(permissions_array),
            cls.unwind_permissions_array_stage(permissions_array),
        ]

    @staticmethod
    def match_user_id_stage(user_id):
        return {"$match": {"user_id": user_id}}

    @staticmethod
    def lookup_group_objects_stage():
        return {
            "$lookup": {
                "from": "groups",
                "localField": "assigned_groups",
                "foreignField": "_id",
                "as": "group_objects",
            }
        }

    @staticmethod
    def unwind_group_objects_stage():
        return {"$unwind": {"path": "$group_objects"}}

    @staticmethod
    def lookup_project_object_for_groups_stage():
        return {
            "$lookup": {
                "from": "projects",
                "localField": "group_objects.project",
                "foreignField": "_id",
                "as": "group_project",
            }
        }

    @staticmethod
    def group_permissions_by_project_stage(permissions_array):
        return {
            "$group": {
                "_id": "$group_project.url",
                permissions_array: {
                    "$addToSet": "$group_objects.{}".format(permissions_array)
                },
            }
        }

    @staticmethod
    def match_project_url_stage(project_url):
        return {"$match": {"_id": project_url}}

    @staticmethod
    def unwind_permissions_array_stage(permissions_array):
        return {"$unwind": {"path": "${}".format(permissions_array)}}

    @staticmethod
    def merge_permissions_stage(permissions_array):
        return {
            "$group": {
                "_id": None,
                permissions_array: {"$addToSet": "${}".format(permissions_array)},
            }
        }

    @staticmethod
    def replace_document_by_pattern_document():
        return {"$replaceRoot": {"newRoot": "$permitted_file_patterns"}}

    @staticmethod
    def group_permissions_by_pattern():
        return {
            "$group": {
                "_id": "$pattern",
                "permitted_statuses": {"$push": "$permitted_statuses"},
            }
        }

    @staticmethod
    def expand_and_merge_status_arrays():
        return {
            "$project": {
                "_id": 0,
                "pattern": "$_id",
                "permitted_statuses": {
                    "$reduce": {
                        "input": "$permitted_statuses",
                        "initialValue": [],
                        "in": {"$setUnion": ["$$value", "$$this"]},
                    }
                },
            }
        }
