from database_manager import DatabaseManager
import fnmatch

COMMON_FILE_STATUS_VIOLATION_MESSAGE = 'Tried to %s file "{}" without permission.'
VERBOSE_FILE_STATUS_VIOLATIONS = {
    "A": "add",
    "C": "copy",
    "D": "delete",
    "M": "modify",
    "R": "rename",
    "T": "change type of",
    "U": "unmerge",
    "X": "stage unknown",
    "B": "stage pairing broken",
}


def get_violations(user_id, project_url, touched_branches, staged_files):
    violations = {"verbose_violations": []}

    violations["verbose_violations"] += verbose_branch_violations(
        get_branch_violations(user_id, project_url, touched_branches)
    )

    if not violations["verbose_violations"]:
        violations["verbose_violations"] += verbose_file_violations(
            get_file_violations(user_id, project_url, staged_files)
        )

    violations["violated"] = bool(violations["verbose_violations"])

    return violations


def get_branch_violations(user_id, project_url, touched_branches):
    forbidden_branches = []

    permitted_branches = DatabaseManager.get_user_permitted_branches(
        user_id, project_url
    )

    for branch in touched_branches:
        if branch not in permitted_branches:
            forbidden_branches.append(branch)

    return forbidden_branches


def get_file_violations(user_id, project_url, staged_files):
    forbidden_files = {}

    permitted_file_patterns = DatabaseManager.get_user_permitted_file_patterns(
        user_id, project_url
    )

    for status, files in staged_files.items():
        for file in files:
            for pattern in permitted_file_patterns:
                if (
                    fnmatch.fnmatch(file, pattern["pattern"])
                    and status in pattern["permitted_statuses"]
                ):
                    break
            else:
                try:
                    forbidden_files[status].append(file)
                except KeyError:
                    forbidden_files[status] = [file]

    return forbidden_files


def verbose_branch_violations(forbidden_branches):
    verbose_violations = []

    for branch in forbidden_branches:
        verbose_violations.append(
            'Tried to modify branch "{}" without permission.'.format(branch)
        )

    return verbose_violations


def verbose_file_violations(forbidden_files):
    verbose_violations = []

    for status, files in forbidden_files.items():
        for file in files:
            verbose_violations.append(
                (
                    COMMON_FILE_STATUS_VIOLATION_MESSAGE
                    % VERBOSE_FILE_STATUS_VIOLATIONS[status]
                ).format(file)
            )

    return verbose_violations
